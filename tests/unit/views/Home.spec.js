import { shallowMount, createLocalVue } from '@vue/test-utils'
import Home from '@/views/Home.vue'
import createStore from '@/store/'

describe('Home.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  it('renders', () => {
    const wrapper = shallowMount(Home, { localVue, store })
    expect(wrapper.text())
  })
})

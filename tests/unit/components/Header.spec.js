import { shallowMount, createLocalVue } from '@vue/test-utils'
import Header from '@/components/Header.vue'
import createStore from '@/store/'

describe('Header.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  test('component exists', () => {
    const wrapper = shallowMount(Header, { localVue, store })
    expect(wrapper.exists()).toBe(true)
  })
})

import { shallowMount, createLocalVue } from '@vue/test-utils'
import DialogForm from '@/components/DialogForm.vue'
import createStore from '@/store/'

describe('DialogForm.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  test('component exists', () => {
    const wrapper = shallowMount(DialogForm, { localVue, store })
    expect(wrapper.exists()).toBe(true)
  })

  test('opens with compatible object that creates a text field', () => {
    const mockData = {
      key: {
        label: 'input label',
        value: ''
      }
    }

    const wrapper = shallowMount(DialogForm, {
      localVue,
      store
    })

    wrapper.vm.open(mockData)

    const inputs = wrapper.findAll('input')
    expect(wrapper.vm.dialog).toBe(true)
    expect(wrapper.vm.object).toBeDefined()
    expect(wrapper.vm.object.key).toBeDefined()
    expect(wrapper.vm.object.key.label).toBe('input label')
    expect(inputs.at(0).exists()).toBe(true)
    expect(inputs.length).toBe(1)
  })

  test('agree with compatible object that creates a text field returns the value of the textfield', () => {
    const mockData = {
      key: {
        label: 'input label',
        value: ''
      }
    }

    const wrapper = shallowMount(DialogForm, {
      localVue,
      store
    })

    wrapper.vm.open(mockData).then((result) => {
      // check results
      expect(result.key).toBeDefined()
      expect(result.key).toBe('hello')
    })
    // simulate input
    mockData.key.value = 'hello'
    // simulate submit click
    wrapper.vm.agree()
    // check if dialog is closed
    expect(wrapper.vm.dialog).toBe(false)
  })
})

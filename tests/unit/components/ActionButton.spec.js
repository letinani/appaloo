import { shallowMount, createLocalVue } from '@vue/test-utils'
import ActionButton from '@/components/ActionButton.vue'
import createStore from '@/store/'

describe('ActionButton.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  test('component exists', () => {
    const wrapper = shallowMount(ActionButton, { localVue, store })
    expect(wrapper.exists()).toBe(true)
  })

  test('renders props.label when passed', () => {
    const label = 'new label'

    const wrapper = shallowMount(ActionButton, {
      localVue,
      store,
      propsData: { label }
    })
    expect(wrapper.text()).toMatch(label)
  })

  test('disabled the button when props.disabled is passed', () => {
    const label = 'new label'
    const disabled = true

    const wrapper = shallowMount(ActionButton, {
      localVue,
      store,
      propsData: { label, disabled }
    })
    const button = wrapper.find('button')
    expect(button.attributes().disabled).toBe('disabled')
  })
})

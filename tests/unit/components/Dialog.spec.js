import { shallowMount, createLocalVue } from '@vue/test-utils'
import Dialog from '@/components/Dialog.vue'
import createStore from '@/store/'

describe('Dialog.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  test('component exists', () => {
    const wrapper = shallowMount(Dialog, { localVue, store })
    expect(wrapper.exists()).toBe(true)
  })
})

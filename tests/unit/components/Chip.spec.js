import { shallowMount, createLocalVue } from '@vue/test-utils'
import Chip from '@/components/Chip.vue'
import createStore from '@/store/'

describe('Chip.vue', () => {
  let store
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    store = createStore()
  })

  test('component exists', () => {
    const wrapper = shallowMount(Chip, { localVue, store })
    expect(wrapper.exists()).toBe(true)
  })

  test('renders props.label when passed', () => {
    const label = 'new label'

    const wrapper = shallowMount(Chip, {
      localVue,
      store,
      propsData: { label }
    })
    expect(wrapper.text()).toMatch(label)
  })
})

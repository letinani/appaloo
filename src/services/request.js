import axios from 'axios'
// import Vue from 'vue'

const REST = {
  baseURL: `api/`,
  withCredentials: false,
  headers: {
    'Accept': 'application/vnd.api+json',
    'Content-Type': 'application/vnd.api+json'
  }
}

const instance = axios.create(REST)

// before a request is made start the loader
instance.interceptors.request.use(
  config => {
    // loader.progress.start()
    return config
  },
  error => {
    console.log('++++', error)
    return Promise.reject(error)
  }
)

// before a response is returned stop the loader
instance.interceptors.response.use(
  response => {
    // loader.progress.done()
    console.log('---', response)
    return response
  },
  error => {
    // handle all kinds of errors with vue notify for example
    if (error.response.status === 400) {
      // handle 400
    }
    return Promise.reject(error)
  }
)

export default instance

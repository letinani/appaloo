import request from '@/services/request'

export function getApplications () {
  return request.get('mobile-applications')
}

export function updateApplication (data) {
  // const formattedData = Object.assign({}, data)
  delete data.hasBeenModified

  const dataJsonApi = { data }
  return request({
    url: `/mobile-applications/${data.id}`,
    method: 'patch',
    data: dataJsonApi
  })
}

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import createStore from './store'

import './styles/reset.styl'

import server from '../mirage'
window.server = server

Vue.config.productionTip = false

const store = createStore()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

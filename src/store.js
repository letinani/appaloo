import Vue from 'vue'
import Vuex from 'vuex'

import { getApplications, updateApplication } from '@/api/mobile-applications'
import { createModule } from 'vuex-toast'
import 'vuex-toast/dist/vuex-toast.css'

Vue.use(Vuex)

export default function createStore () {
  return new Vuex.Store({
    modules: {
      toast: createModule({
        dismissInterval: 9000
      })
    },
    state: {
      isLoading: true,
      apps: [],
      currentAppId: null,
      searchWord: null,
      searchDevice: null,
      filteredApps: []
    },
    getters: {
      appDetails: (state) => {
        if (state.currentAppId && state.apps.length > 0) {
          const result = state.apps.find(app => parseInt(app.id) === parseInt(state.currentAppId))
          return result
        }
        return {}
      },
      filteredAppDevice: (state) => {
        if (state.searchWord && state.searchWord.length > 0) {
          return state.apps.filter((app) =>
            app.attributes.name.toLowerCase().includes(state.searchWord) || app.attributes.catchphrase.toLowerCase().includes(state.searchWord)
          )
        } else if (state.searchDevice && state.searchDevice.length > 0) {
          return state.apps.filter((app) => {
            const testedString = app.attributes['os-type'].toLowerCase()
            return testedString.includes(state.searchDevice) || testedString.includes('all')
          })
        } else {
          return state.apps
        }
      },
      getMetaTotals: (state) => {
        return {
          total: state.apps.length,
          totalIos: state.apps.filter((app) => app.attributes['os-type'] === 'ios').length,
          totalAndroid: state.apps.filter((app) => app.attributes['os-type'] === 'android').length
        }
      },
      getRecents: (state) => {
        const sortedApps = state.apps.sort((a, b) => new Date(b.attributes['created-at']) - new Date(a.attributes['created-at']))
        return sortedApps.slice(0, 3)
      }
    },
    mutations: {
      /* mutations on project */
      SET_APPS: (state, payload) => {
        state.apps = payload
        state.filteredApps = payload
      },
      SET_CURRENT_APP_ID: (state, id) => {
        state.currentAppId = parseInt(id)
      },
      UPDATE_SRCH_INPUT: (state, input) => {
        const word = input.trim().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
        state.searchWord = word
      },
      UPDATE_DEVICE_INPUT: (state, input) => {
        const word = input ? input.trim().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') : null
        state.searchDevice = word
      },
      UPDATE_APP_STATUS: (state, payload) => {
        const app = state.apps.find(app => parseInt(app.id) === parseInt(state.currentAppId))
        app.hasBeenModified = payload
      },
      SET_LOADING: (state, value) => {
        state.isLoading = value
      }
    },
    actions: {
      GET_APPS: async ({ commit }, options) => {
        commit('SET_LOADING', true)
        const { data: { data } } = await getApplications()
        commit('SET_LOADING', false)
        commit('SET_APPS', data)
      },
      SHOW_DETAILS (context, index) {
        const id = parseInt(index)
        if (!index) context.commit('SET_CURRENT_APP_ID', null)
        if (id < context.state.apps.length) {
          context.commit('SET_CURRENT_APP_ID', id)
        }
      },
      MODIFY_APP_DRAFT ({ commit }) {
        commit('UPDATE_APP_STATUS', true)
      },
      SAVE_APP: async (state) => {
        const app = state.getters.appDetails
        const data = await updateApplication(app)
        state.commit('UPDATE_APP_STATUS', false)
        // console.log(app, data)
      }
    }
  })
}

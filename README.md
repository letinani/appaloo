# mini-appaloo-vuejs v2


## requirements
* Node
On Macosx install it using brew `brew install node`
or follow the steps on the official [website](https://nodejs.org)

## Quick overview
Base on a classical VueJS architecture using: 
* Vuex
* Vue Router
* Stylus as a css preprocessor
* Axios
* Jest

## Project setup and run on local serveur
```
cd mini-appaloo-vuejs
npm install
npm start
```
Then open the Network link on desktop browser


### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run tests
```
npm run test:uni
```

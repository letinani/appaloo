const path = require('path')

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/appaloo/'
    : '/',
  lintOnSave: false,
  // chainWebpack: config => {
  //   config.module
  //     .rule('vue')
  //     .use('vue-svg-inline-loader')
  //     .loader('vue-svg-inline-loader')
  //     .options({ /* ... */ })
  // },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'stylus',
      patterns: [
        path.resolve(__dirname, './src/styles/tools.styl')
      ]
    }
  }
}
